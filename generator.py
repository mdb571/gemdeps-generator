#!/usr/bin/env python

import yaml
import gemdeps
import urlparse
import urllib2
import traceback

with open('softwares.yml', 'r') as filecontent:
    softwares = yaml.load(filecontent, Loader=yaml.FullLoader)

for software, attributes in softwares.items():
    try:
        version = attributes['version']
        print "Processing %s-%s" % (software, version)
        url = urlparse.urljoin(attributes['url'], 'raw')
        url = url + "/" + version
        gemfile = url + "/Gemfile"
        gemfile_lock = url + "/Gemfile.lock"
        print "Downloading Gemfile"
        with open("%s-%s_Gemfile" % (software, version), 'w') as f:
            print gemfile
            opener = urllib2.build_opener()
            opener.addheaders = [('User-Agent', 'Mozilla/5.0')]
            response = opener.open(gemfile)
            content = response.read()
            f.write(content)
        print "Downloading Gemfile.lock"
        with open("%s-%s_Gemfile.lock" % (software, version), 'w') as f:
            print gemfile_lock
            opener = urllib2.build_opener()
            opener.addheaders = [('User-Agent', 'Mozilla/5.0')]
            response = opener.open(gemfile_lock)
            content = response.read()
            f.write(content)
        if 'gemspec' in attributes:
            gemspec = url + "/" + attributes['gemspec']
            print "Downloading gemspec"
            with open("%s-%s.gemspec" % (software, version), 'w') as f:
                print gemspec
                opener = urllib2.build_opener()
                opener.addheaders = [('User-Agent', 'Mozilla/5.0')]
                response = opener.open(gemspec)
                content = response.read()
                f.write(content)
        print "Triggering gemdeps"
        obj = gemdeps.GemDeps("%s-%s" % (software, version), True, 'exceptions.yaml', 'skip.yaml')
        obj.process("%s-%s_Gemfile" % (software, version))
        obj.write_output('data')
        obj.generate_dot('data')
    except Exception as e:
        print(e)
        traceback.print_exc()
        continue
